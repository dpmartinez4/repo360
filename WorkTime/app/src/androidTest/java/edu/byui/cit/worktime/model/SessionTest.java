package edu.byui.cit.worktime.model;

import android.content.Context;

import androidx.test.platform.app.InstrumentationRegistry;

import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class SessionTest {

    Context appCtx = InstrumentationRegistry.getInstrumentation().getTargetContext();
    AppDatabase db = AppDatabase.getInstance(appCtx);
    SessionDAO sdao = db.getSessionDAO();
    ProjectDAO pdao = db.getProjectDAO();
    private Object Project;

    @Test
    public void testGetSessionClass() {
        pdao.deleteAll(); //added
        Project proj1 = new Project("Write Essay", "English301");
        pdao.insert(proj1);
        assertEquals(1, pdao.count());

        Session sess1 = new Session(proj1.getProjectKey(), "English301", new Date(2021-02-02), new Date(2021-02-02));
        Session sess2 = new Session(proj1.getProjectKey(), "PlayZelda", new Date(2021-02-02), new Date(2021-02-02));
        Session sess3 = new Session(proj1.getProjectKey(), "Keep playing", new Date(2021-02-02), new Date(2021-02-02));

        assertEquals(0, sess1.getSessionKey());
        assertEquals("English301", sess1.getDescription());
        assertFalse(sess1.equals(sess2));

        //verify that the count is 2
        sdao.deleteAll();
        sdao.insert(sess1);
        sdao.insert(sess2);
        sdao.insert(sess3);
        assertEquals(3, sdao.count());
        //retrieves insert rows**
        Session stored1 = sdao.getSessionBySessionKey(sess1.getSessionKey());
        Session stored2 = sdao.getSessionBySessionKey(sess2.getSessionKey());
        Session stored3 = sdao.getSessionBySessionKey(sess3.getSessionKey());
        //verify if the retrieved data matches
        assertEquals(sess1, stored1);
        assertEquals(sess2, stored2);
        assertEquals(sess3, stored3);
        //update one of the inserted rows
        sess1.setDescription("English320");
        sdao.update(sess1);
        //verify that the count is still 2
        assertEquals(3, sdao.count());
        //retrieve the updated row
        Session updated1 = sdao.getSessionBySessionKey(sess1.getSessionKey());
        //verify that the data in the retrieved row is exactly the same as the data that was updated into the row
        assertEquals(sess1, updated1);
        //retrieve the no updated row
        Session stored4 = sdao.getSessionBySessionKey(sess2.getSessionKey());
        Session stored5 = sdao.getSessionBySessionKey(sess3.getSessionKey());
        //verify the no update rows
        assertEquals(sess2, stored4);
        assertEquals(sess3, stored5);
        //delete one of the rows
        sdao.delete(sess2);
        sdao.delete(sess3);
        //verify that the count is 1
        assertEquals(1, sdao.count());
        //retrieve the row that is still in the database
        Session stored6 = sdao.getSessionBySessionKey(sess1.getSessionKey());
        //verify that the remaining row contains the correct data
        assertEquals(sess1, stored6);
        //delete the remaining row
        sdao.deleteAll();
        //verify that the count is 0
        assertEquals(0, sdao.count());
    }
}