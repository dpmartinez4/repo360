package edu.byui.cit.worktime.model;

import android.content.Context;

import androidx.test.platform.app.InstrumentationRegistry;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ProjectTest {

    @Test
    public void testGetProjectClass() {
        Project proj1 = new Project("Write Essay", "English301");
        Project proj2 = new Project("PlayZelda", "FindThePrincess");
        Project proj3 = new Project("Write Essay", "English301");

        assertEquals(0, proj1.getProjectKey());
        assertEquals("Write Essay", proj1.getTitle());
        assertEquals("English301", proj1.getDescription());

        assertFalse(proj1.equals(proj2));
        assertTrue(proj1.equals(proj3));

        Context appCtx = InstrumentationRegistry.getInstrumentation().getTargetContext();
        AppDatabase db = AppDatabase.getInstance(appCtx);
        ProjectDAO pdao = db.getProjectDAO();

        pdao.deleteAll();

        assertEquals(0, pdao.count());

        pdao.insert(proj1);
        assertEquals(1, pdao.count());

        pdao.insert(proj2);
        assertEquals(2, pdao.count());
        //retrieves insert rows
        Project stored1 = pdao.getProjectByProjectKey(proj1.getProjectKey());
        Project stored2 = pdao.getProjectByProjectKey(proj2.getProjectKey());
        //verify if the retrieved data matches
        assertEquals(proj1, stored1);
        assertEquals(proj2, stored2);
        //update one of the inserted rows
        proj1.setDescription("English320");
        pdao.update(proj1);
        //verify the count is still 2
        assertEquals(2, pdao.count());
        //retrieve the updated row
        Project updated1 = pdao.getProjectByProjectKey(proj1.getProjectKey());
        //retrieve that retrieved data is exactly the data
        assertEquals(proj1, updated1);

    }
}