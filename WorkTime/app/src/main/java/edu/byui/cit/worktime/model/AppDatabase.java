package edu.byui.cit.worktime.model;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import edu.byui.cit.worktime.model.Project;
import edu.byui.cit.worktime.model.ProjectDAO;
import edu.byui.cit.worktime.model.Session;
import edu.byui.cit.worktime.model.SessionDAO;


@Database(entities = { Project.class, Session.class }, version = 1, exportSchema = false)
@TypeConverters({ Converters.class })
public abstract class AppDatabase extends RoomDatabase {
    private static AppDatabase singleton;

    public static AppDatabase getInstance(Context appCtx) {
        if (singleton == null) {
            singleton = Room.databaseBuilder(
                    appCtx, AppDatabase.class, "WorkTime")
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return singleton;
    }

    public abstract ProjectDAO getProjectDAO();
    public abstract SessionDAO getSessionDAO();
}
