package edu.byui.cit.worktime.controller;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import edu.byui.cit.worktime.R;
import edu.byui.cit.worktime.model.AppDatabase;
import edu.byui.cit.worktime.model.Project;
import edu.byui.cit.worktime.model.ProjectDAO;

public class AddProjectFrag extends Fragment {
@Override
        public View onCreateView(@NonNull LayoutInflater inflater,
                                 ViewGroup container, Bundle savedInstState) {
            View view = null;
            try {
                super.onCreateView(inflater, container, savedInstState);

                // Load the xml file that corresponds to this Java file.
                view = inflater.inflate(R.layout.frag_add_project, container, false);
                //Find the add button and link it to the click listener
                Button btnAdd = view.findViewById(R.id.button13);
                btnAdd.setOnClickListener(new HandleAddClick());
                //Find the cancel button and link it to the click listener
                Button btnCancel = view.findViewById(R.id.button14);
                btnCancel.setOnClickListener(new HandleCancelClick());

            }
            catch (Exception ex) {
                Log.e(MainActivity.TAG, ex.toString());
            }
            return view;
        }

    private final class HandleAddClick implements View.OnClickListener {

        @Override
        public void onClick(View button) {
            View view = getView();
            assert view != null;
            EditText edtTitle = view.findViewById(R.id.edtTitle);
            String title = edtTitle.getText().toString();

            EditText edtDescrip = view.findViewById(R.id.edtDescrip);
            String descrip = edtDescrip.getText().toString();

            Project newProj = new Project(title, descrip);

            Activity act = getActivity();
            assert act != null;
            Context appCtx = act.getApplicationContext();
            AppDatabase db = AppDatabase.getInstance(appCtx);
            ProjectDAO pdao = db.getProjectDAO();

            pdao.insert(newProj);

            //Simulate the user pressing the back arrow

            act.onBackPressed();

        }
    }
        private final class HandleCancelClick implements View.OnClickListener {

            @Override
            public void onClick(View button) {
        //Simulate the user pressing the back arrow
        Activity act = getActivity();
        assert act !=null;
        act.onBackPressed();

            }
        }

        }

