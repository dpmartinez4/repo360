package edu.byui.cit.worktime.controller;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import edu.byui.cit.worktime.R;


public final class MainFrag extends Fragment {

        @Override
        public View onCreateView(@NonNull LayoutInflater inflater,
                                 ViewGroup container, Bundle savedInstState) {
            View view = null;
            try {
                super.onCreateView(inflater, container, savedInstState);

                // Load the xml file that corresponds to this Java file.
                view = inflater.inflate(R.layout.frag_main, container, false);

                MainActivity act = (MainActivity)getActivity();
                RecyclerView recycler = view.findViewById(R.id.recycler);
                recycler.setHasFixedSize(true); //added
                recycler.setLayoutManager(new LinearLayoutManager(act));


                ProjectAdapter adapter = new ProjectAdapter(act);
                recycler.setAdapter(adapter);

                // Get the floating action button and add a function to it.
                FloatingActionButton fabAddProject = view.findViewById(R.id.fabAddProject);
                // This is your on click listener
                fabAddProject.setOnClickListener(new HandleFABClick());

            }
            catch (Exception ex) {
                Log.e(MainActivity.TAG, ex.toString());
            }
            return view;
        }

        private class HandleFABClick implements View.OnClickListener {

            @Override
            public void onClick(View v) {
                // create a new project fragment and display it for the user
                MainActivity act = (MainActivity) getActivity();
                assert act != null;
                AddProjectFrag frag = new AddProjectFrag();
                FragmentTransaction trans =
                        act.getSupportFragmentManager().beginTransaction();
                trans.replace(R.id.fragContainer, frag);
                 // Add this fragment to the back stack so we can return with the back arrow
                trans.addToBackStack(null);
                trans.commit();
//                Context ctx = getActivity();
//                Toast.makeText(ctx, "FAB Clicked", Toast.LENGTH_LONG).show();
            }
        }
    }

